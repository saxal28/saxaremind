//
//  TodoCell.swift
//  SaxaRemind
//
//  Created by Alan Sax on 1/25/19.
//  Copyright © 2019 Alan Sax. All rights reserved.
//

import UIKit
import RealmSwift
import Jelly
//import SwiftySound


class TodoCell: UICollectionViewCell {
    
    let realm = try! Realm()
    var animator: Jelly.Animator?

    var todo: Todo? {
        didSet {
            guard let unwrappedTodo = todo else {return}
            setupCellContent(unwrappedTodo: unwrappedTodo)
            handleTodoProgress()
        }
    }
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        setupLayout()
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    // =========================
    // UI Components
    // =========================
    
    lazy var todoContainer : UIView = {
        let view = UIView()
        view.translatesAutoresizingMaskIntoConstraints = false
        view.clipsToBounds = false
        view.backgroundColor = .todoTrackColor
        view.layer.cornerRadius = 30
        
        view.layer.shadowOpacity = 0.4
        view.layer.shadowOffset = CGSize(width: 0, height: 2)
        view.layer.shadowRadius = 2.0
        view.layer.shadowColor = UIColor.black.cgColor
        
        view.addSubview(todoProgress)
        view.addSubview(todoName)
        view.addSubview(todoStatus)
                
        return view
    }()
    
    let todoProgress: UIView = {
        
        let view = UIView()
        view.translatesAutoresizingMaskIntoConstraints = false
        view.backgroundColor = .todoProgressColor
        view.layer.cornerRadius = 30
        return view
    }()
    
    let leftActionButtonSize: CGFloat = 70
    lazy var todoLeftActionButton: UIButton = {
        let button = UIButton()
        button.translatesAutoresizingMaskIntoConstraints = false
        button.heightAnchor.constraint(equalToConstant: leftActionButtonSize).isActive = true
        button.widthAnchor.constraint(equalToConstant: leftActionButtonSize).isActive = true
        button.alpha = 2
        button.backgroundColor = .todoButtonColor
        button.layer.masksToBounds = false
        button.layer.cornerRadius = leftActionButtonSize / 2
        button.addTarget(self, action: #selector(handleLeftActionButtonPress), for: .touchUpInside)
        button.setImage(UIImage(named: "check"), for: .normal)
        
        button.layer.shadowOpacity = 0.4
        button.layer.shadowOffset = CGSize(width: 0, height: 2)
        button.layer.shadowRadius = 2.0
        button.layer.shadowColor = UIColor.black.cgColor
        
        button.layer.borderWidth = 3
        button.layer.borderColor = UIColor.white.cgColor
    
        return button
    }()
    
    let rightActionButtonSize: CGFloat = 50
    lazy var todoRightActionButton: UIButton = {
        let button = UIButton()
        button.translatesAutoresizingMaskIntoConstraints = false
        button.heightAnchor.constraint(equalToConstant: rightActionButtonSize).isActive = true
        button.widthAnchor.constraint(equalToConstant: rightActionButtonSize).isActive = true
        button.backgroundColor = .todoButtonColor
        button.layer.cornerRadius = rightActionButtonSize / 2
        button.addTarget(self, action: #selector(handleRightActionButtonPress), for: .touchUpInside)
        button.setImage(UIImage(named: "minus"), for: .normal)
        
        button.layer.shadowOpacity = 0.4
        button.layer.shadowOffset = CGSize(width: 0, height: 2)
        button.layer.shadowRadius = 2.0
        button.layer.shadowColor = UIColor.black.cgColor
        
        return button
    }()
    
    let menuButtonSize: CGFloat = 28
    lazy var menuButton: UIButton = {
        let button = UIButton()
        button.translatesAutoresizingMaskIntoConstraints = false
        button.widthAnchor.constraint(equalToConstant: menuButtonSize).isActive = true
        button.heightAnchor.constraint(equalToConstant: menuButtonSize).isActive = true
        button.backgroundColor = .todoButtonColor
        button.layer.cornerRadius = menuButtonSize / 2
        button.setImage(UIImage(named: "elipsis"), for: .normal)
        
        button.addTarget(self, action: #selector(handleMenuPress), for: .touchUpInside)
        
        button.layer.shadowOpacity = 0.4
        button.layer.shadowOffset = CGSize(width: 0, height: 2)
        button.layer.shadowRadius = 2.0
        button.layer.shadowColor = UIColor.black.cgColor
        return button
    }()
    
    let todoName: UILabel = {
        let label = UILabel()
        label.translatesAutoresizingMaskIntoConstraints = false
        label.font = UIFont(name: "AvenirNext-Bold", size: 20)
        label.textColor = .white
        return label
    }()
    
    let todoStatus: UILabel = {
        let label = UILabel()
        label.text = "0/1 Completed"
        label.font = UIFont(name: "AvenirNext-DemiBold", size: 12)
        label.textColor = .white
        label.translatesAutoresizingMaskIntoConstraints = false
        return label
    }()
    
    func setupCellContent(unwrappedTodo: Todo) {
        
        todoName.text = unwrappedTodo.name
        
        if(unwrappedTodo.max > 1) {
            todoStatus.text = "\(unwrappedTodo.current)/\(unwrappedTodo.max) Completed"
        } else {
            todoStatus.text = unwrappedTodo.current == unwrappedTodo.max ? "Completed" : "Not Completed"
        }
        
    }
    
    // setup views
    func setupLayout() {
        
        self.addSubview(todoContainer)
        self.addSubview(todoLeftActionButton)
        self.addSubview(todoRightActionButton)
        self.addSubview(menuButton)
        
        print("SETUP CELL!")

        todoContainer.topAnchor.constraint(equalTo: topAnchor).isActive = true
        todoContainer.leftAnchor.constraint(equalTo: leftAnchor, constant: 70).isActive = true
        todoContainer.rightAnchor.constraint(equalTo: rightAnchor, constant: -40).isActive = true
        todoContainer.bottomAnchor.constraint(equalTo: bottomAnchor).isActive = true
        
        todoLeftActionButton.rightAnchor.constraint(equalTo: todoContainer.leftAnchor, constant: leftActionButtonSize / 2).isActive = true
        todoLeftActionButton.centerYAnchor.constraint(equalTo: todoContainer.centerYAnchor).isActive = true
        
        todoRightActionButton.rightAnchor.constraint(equalTo: todoContainer.rightAnchor, constant: rightActionButtonSize / 2).isActive = true
        todoRightActionButton.centerYAnchor.constraint(equalTo: todoContainer.centerYAnchor).isActive = true
        
        menuButton.rightAnchor.constraint(equalTo: todoContainer.rightAnchor, constant: menuButtonSize / 2 - 10).isActive = true
        menuButton.topAnchor.constraint(equalTo: todoContainer.topAnchor, constant: menuButtonSize / 2 * -1).isActive = true
        
        todoProgress.topAnchor.constraint(equalTo: todoContainer.topAnchor).isActive = true
        todoProgress.bottomAnchor.constraint(equalTo: todoContainer.bottomAnchor).isActive = true
        todoProgress.leftAnchor.constraint(equalTo: todoContainer.leftAnchor).isActive = true
        
        todoName.centerYAnchor.constraint(equalTo: todoContainer.centerYAnchor, constant: -5).isActive = true
        todoName.leadingAnchor.constraint(equalTo: todoContainer.leadingAnchor, constant: 45).isActive = true
        todoName.trailingAnchor.constraint(equalTo: todoContainer.trailingAnchor, constant: 45).isActive = true

        todoStatus.topAnchor.constraint(equalTo: todoName.bottomAnchor, constant: -3).isActive = true
        todoStatus.leadingAnchor.constraint(equalTo: todoName.leadingAnchor).isActive = true
        
        print("FINISHED SETTING UP CELL!")
        
    }
    
    
    // ======================================
    // MARK: SAVING FUNCTIONS
    // ======================================
    
    
    private func updateAndSaveTodo(key: String, value: Any) {
        try! realm.write {
            todo![key] = value
        }
        
        realm.refresh()
        
        print("TODO \(key) updated!", value)
    }

    
    // ======================================
    // MARK: TODO ACTION HANDLERS
    // ======================================
    
    
    @objc private func handleLeftActionButtonPress() {
        
        guard let unwrappedTodo = todo else { return }
        let todoInProgress = unwrappedTodo.current < unwrappedTodo.max
        
//        Sound.play(file: "boop.mp3")
    
        
        if(todoInProgress) {
            let value = unwrappedTodo.current + 1;
            updateAndSaveTodo(key: "current", value: value)
            setupCellContent(unwrappedTodo: unwrappedTodo)
            handleTodoProgress()
        }
    }
    
    @objc private func handleRightActionButtonPress() {
        guard let unwrappedTodo = todo else { return }
        let todoStarted = unwrappedTodo.current > 0
        let todoUnCompleted = unwrappedTodo.current == unwrappedTodo.max
        
        if(todoStarted) {
            let value = unwrappedTodo.current - 1;
            updateAndSaveTodo(key: "current", value: value)
            todoLeftActionButton.setImage(UIImage(named: "check"), for: .normal)
            setupCellContent(unwrappedTodo: unwrappedTodo)
            handleTodoProgress()
            
            
            if todoUnCompleted {
                todoProgress.backgroundColor = .todoProgressColor
                todoLeftActionButton.layer.borderColor = UIColor.white.cgColor
            }
        }
    }
    
    @objc func handleMenuPress() {
//        guard let unwrappedTodo = todo else { return }
//        let viewController = AddTodoFormVC()
//        viewController.todoToEdit = unwrappedTodo
//
//        let allCorners: CACornerMask = [.layerMaxXMaxYCorner,.layerMaxXMinYCorner,.layerMinXMaxYCorner,.layerMinXMinYCorner]
//        let uiConfiguration = PresentationUIConfiguration(cornerRadius: 10,
//                                                          backgroundStyle: .blurred(effectStyle: .dark),
//                                                          isTapBackgroundToDismissEnabled: true,
//                                                          corners: allCorners )
//
//        let size = PresentationSize(width: .fullscreen, height: .custom(value: 522))
//
//        let alignment = PresentationAlignment(vertical: .center, horizontal: .center)
//
//        let marginGuards = UIEdgeInsets(top: 0, left: 25, bottom: 0, right: 25)
//
//        let timing = PresentationTiming(duration: .normal, presentationCurve: .linear, dismissCurve: .linear)
//
//        let interactionConfiguration = InteractionConfiguration(presentingViewController: tableView,
//                                                                completionThreshold: 0.4,
//                                                                dragMode: .canvas)
//
//        let presentation = CoverPresentation(directionShow: .bottom,
//                                             directionDismiss: .bottom,
//                                             uiConfiguration: uiConfiguration,
//                                             size: size,
//                                             alignment: alignment,
//                                             marginGuards: marginGuards,
//                                             timing: timing,
//                                             spring: .none,
//                                             interactionConfiguration: interactionConfiguration)
//
//        animator = Animator(presentation:presentation)
//        animator?.prepare(presentedViewController: viewController)
//        present(viewController, animated: true, completion: nil)
    }
    
    // ======================================
    // MARK: TODO PROGRESS
    // ======================================
    
    func handleTodoProgress() {
        guard let unwrappedTodo = todo else { return }
        let todoCompleted = unwrappedTodo.current == unwrappedTodo.max
        
        self.layoutIfNeeded()
        
        
        let frame = CGRect(x: 0, y: 0, width: todoContainer.frame.width * (CGFloat(unwrappedTodo.current)) / CGFloat(unwrappedTodo.max), height: todoContainer.frame.height)

        UIView.animate(withDuration: 0.2) {
            self.todoProgress.frame = frame
        }

        if todoCompleted {
            print(self.bounds.width, self.frame.width)
            self.todoProgress.backgroundColor = .todoCompletedColor

            UIView.animate(withDuration: 0.2, delay: 0.15, options: [], animations: {
                self.todoProgress.backgroundColor = .todoCompletedColor
                self.todoLeftActionButton.setImage(UIImage(named: "check-disabled"), for: .normal)
                self.todoLeftActionButton.layer.borderColor = UIColor.todoCompletedColor.cgColor
            })

        }
    }
}
