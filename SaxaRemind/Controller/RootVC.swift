//
//  RootVC.swift
//  SaxaRemind
//
//  Created by Alan Sax on 2/2/19.
//  Copyright © 2019 Alan Sax. All rights reserved.
//

import UIKit

class RootVC: UINavigationController {

    override func viewDidLoad() {
        super.viewDidLoad()
        
        let button = UIButton()
        button.translatesAutoresizingMaskIntoConstraints = false
        button.frame = CGRect(x: 0, y: 0, width: 200, height: 200)
        button.titleLabel?.text = "Go"
        button.backgroundColor = .blue
        button.addTarget(self, action: #selector(testNavigate), for: .touchUpInside)
        
        view.addSubview(button)
        
        button.centerXAnchor.constraint(equalTo: view.centerXAnchor).isActive = true
        button.centerYAnchor.constraint(equalTo: view.centerYAnchor).isActive = true
    }
    
    @objc func testNavigate() {
        print("click")
                let layout = UICollectionViewLayout()
        let newViewController = TodosCollectionVC(collectionViewLayout: layout)
        self.present(newViewController, animated: true)
    }

}
