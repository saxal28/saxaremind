//
//  HeaderVC.swift
//  SaxaRemind
//
//  Created by Alan Sax on 2/2/19.
//  Copyright © 2019 Alan Sax. All rights reserved.
//


import UIKit
import Jelly

class HeaderVC: UIViewController {
    
    var animator: Jelly.Animator?
    
    let headerWrapper: UIView = {
        let container = UIView()
        container.translatesAutoresizingMaskIntoConstraints = false
        return container
    }()
    
    let avatar: UIImageView = {
        let imageView = UIImageView()
        imageView.translatesAutoresizingMaskIntoConstraints = false
        imageView.layer.borderWidth = 2
        imageView.layer.borderColor = UIColor.white.cgColor
        imageView.layer.cornerRadius = 47.5
        imageView.widthAnchor.constraint(equalToConstant: 95).isActive = true
        imageView.heightAnchor.constraint(equalToConstant: 95).isActive = true
        
        imageView.layer.shadowOpacity = 0.4
        imageView.layer.shadowOffset = CGSize(width: 0, height: 2)
        imageView.layer.shadowRadius = 2.0
        imageView.layer.shadowColor = UIColor.black.cgColor
        
        imageView.image = UIImage(named: "avatar")
        
        return imageView
    }()
    
    let settingsButton: UIButton = {
        let button = UIButton()
        button.translatesAutoresizingMaskIntoConstraints = false
        button.heightAnchor.constraint(equalToConstant: 50).isActive = true
        button.widthAnchor.constraint(equalToConstant: 50).isActive = true
        button.setImage(UIImage(named: "settings"), for: .normal)
        return button
    }()
    
    let addItemButton: UIButton = {
        let button = UIButton()
        button.translatesAutoresizingMaskIntoConstraints = false
        button.setTitle("ADD ITEM", for: .normal)
        button.titleLabel?.font = UIFont(name: "AvenirNext-Bold", size: 15)
        button.addTarget(self, action: #selector(handleAddItemPress), for: .touchUpInside)
        
        return button
    }()

    override func viewDidLoad() {
        super.viewDidLoad()
        
        view.addSubview(headerWrapper)
        headerWrapper.addSubview(avatar)
        headerWrapper.addSubview(settingsButton)
        headerWrapper.addSubview(addItemButton)
        
        headerWrapper.topAnchor.constraint(equalTo: view.safeAreaLayoutGuide.topAnchor).isActive = true
        headerWrapper.leftAnchor.constraint(equalTo: view.leftAnchor).isActive = true
        headerWrapper.rightAnchor.constraint(equalTo: view.rightAnchor).isActive = true
        headerWrapper.bottomAnchor.constraint(equalTo: view.bottomAnchor).isActive = true
    
        avatar.topAnchor.constraint(equalTo: headerWrapper.topAnchor, constant: 20).isActive = true
        avatar.leftAnchor.constraint(equalTo: headerWrapper.leftAnchor, constant: 40).isActive = true
        
        settingsButton.topAnchor.constraint(equalTo: headerWrapper.topAnchor, constant: 10).isActive = true
        settingsButton.rightAnchor.constraint(equalTo: headerWrapper.rightAnchor, constant: -20).isActive = true
        
        addItemButton.bottomAnchor.constraint(equalTo: avatar.bottomAnchor, constant: -5).isActive = true
        addItemButton.rightAnchor.constraint(equalTo: headerWrapper.rightAnchor, constant: -30).isActive = true

    }
    
    @objc func handleAddItemPress() {
        let viewController = AddTodoFormVC()
        
        let allCorners: CACornerMask = [.layerMaxXMaxYCorner,.layerMaxXMinYCorner,.layerMinXMaxYCorner,.layerMinXMinYCorner]
        let uiConfiguration = PresentationUIConfiguration(cornerRadius: 10,
                                                          backgroundStyle: .blurred(effectStyle: .dark),
                                                          isTapBackgroundToDismissEnabled: true,
                                                          corners: allCorners )
        
        let size = PresentationSize(width: .fullscreen, height: .custom(value: 522))
        
        let alignment = PresentationAlignment(vertical: .center, horizontal: .center)
        
        let marginGuards = UIEdgeInsets(top: 0, left: 25, bottom: 0, right: 25)
        
        let timing = PresentationTiming(duration: .normal, presentationCurve: .linear, dismissCurve: .linear)
        
        let interactionConfiguration = InteractionConfiguration(presentingViewController: self,
                                                                completionThreshold: 0.4,
                                                                dragMode: .canvas)
        
        let presentation = CoverPresentation(directionShow: .bottom,
                                             directionDismiss: .bottom,
                                             uiConfiguration: uiConfiguration,
                                             size: size,
                                             alignment: alignment,
                                             marginGuards: marginGuards,
                                             timing: timing,
                                             spring: .none,
                                             interactionConfiguration: interactionConfiguration)
        
        animator = Animator(presentation:presentation)
        animator?.prepare(presentedViewController: viewController)
        present(viewController, animated: true, completion: nil)
        
        
        
    }

}
