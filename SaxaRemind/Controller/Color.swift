//
//  Color.swift
//  SaxaRemind
//
//  Created by Alan Sax on 2/2/19.
//  Copyright © 2019 Alan Sax. All rights reserved.
//

import UIKit

extension UIColor {

    static let backgroundColor       = #colorLiteral(red: 0.2392156863, green: 0.2392156863, blue: 0.2392156863, alpha: 1)
    static let todoButtonColor       = #colorLiteral(red: 0.2392156863, green: 0.2392156863, blue: 0.2392156863, alpha: 1)
    static let todoTrackColor        = #colorLiteral(red: 0.431372549, green: 0.7450980392, blue: 0.6745098039, alpha: 1)
    static let todoProgressColor     = #colorLiteral(red: 0, green: 0.7389556766, blue: 0.6072036028, alpha: 1)
    static let todoCompletedColor    = #colorLiteral(red: 0, green: 0.7389556766, blue: 0.6072036028, alpha: 1)
    
}
