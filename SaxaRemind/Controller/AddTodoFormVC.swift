//
//  AddTodoFormVC.swift
//  SaxaRemind
//
//  Created by Alan Sax on 2/19/19.
//  Copyright © 2019 Alan Sax. All rights reserved.
//

import UIKit
import Eureka
import RealmSwift

class AddTodoFormVC: FormViewController {
    
    var todoToEdit: Todo?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        form +++ Section() { section in
            var header = HeaderFooterView(.class)
            header.height = {55}
            header.onSetupView = { view, _ in
                view.backgroundColor = UIColor.todoCompletedColor
                let title = UILabel()
                title.text = "Add Todo"
                title.font = UIFont(name: "AvenirNext-Regular", size: 26)
                title.textColor = .white
                title.frame = CGRect(x: 15, y: 3, width: 200, height: 55)
                
                view.addSubview(title)
            }
            section.header = header
        }
            <<< TextRow(){ row in
                row.title = "name"
                row.placeholder = "todo name"
                row.tag = "name"
                row.add(rule: RuleRequired())
                row.value = todoToEdit?.name ?? nil
                row.validationOptions = .validatesOnChange
            }
            .cellUpdate { cell, row in
                if !row.isValid {
                    cell.titleLabel?.textColor = .red
                }
            }
            <<< IntRow(){ row in
                row.title = "frequency"
                row.placeholder = "total"
                row.tag = "max"
                row.value = todoToEdit?.max ?? nil
            }
            <<< DateTimeRow(){ row in
                row.title = "Due Date"
                row.tag = "dueDate"
                row.value = todoToEdit?.dueDate ?? Date()
            }
            <<< SwitchRow() { row in
                row.title = "Everyday"
                row.tag = "isEveryday"
                row.value = todoToEdit?.isEveryDay ?? nil
            }
            <<< SwitchRow() { row in
                row.title = "Recurring"
                row.tag = "isRecurring"
                row.value = todoToEdit?.isRecurring ?? nil
            }
            <<< SwitchRow() { row in
                row.title = "Send Notifications"
                row.tag = "canSendNotifications"
                row.value = todoToEdit?.canSendNotifications ?? nil
            }
            <<< TimeRow(){ row in
                row.title = "Notification Time"
                row.tag = "notificationTime"
                row.value = todoToEdit?.notificationTime ?? nil
            }
            <<< TextAreaRow(){ row in
                row.title = "description"
                row.placeholder = "todo description"
                row.tag = "descriptionText"
                row.value = todoToEdit?.descriptionText ?? nil
            }
        
        tableView.bounces = false
        
    }
    
    override func tableView(_ tableView: UITableView, viewForFooterInSection section: Int) -> UIView? {
        let submitButton = UIButton(frame: CGRect(x: 0, y: 0, width: tableView.frame.size.width, height: 50))
        submitButton.backgroundColor = UIColor.todoCompletedColor
        submitButton.setTitle("Add Todo", for: .normal)
        submitButton.addTarget(self, action: #selector(handleSubmitButtonPress), for: .touchUpInside)
        return submitButton
    }
    
    override func tableView(_ tableView: UITableView, heightForFooterInSection section: Int) -> CGFloat {
        return 50
    }
    
    @objc func handleSubmitButtonPress() {
        print("HANDLE SUBMIT!", form.values())
        
        if(todoToEdit != nil) {
            print("EDIT TODO")
        } else {
            addNewTodo()
        }
    }
    
    func addNewTodo() {
        let error = form.validate()
        
        if(error.count > 0) {
            print("max",  form.rowBy(tag: "max")!.value ?? 0)
            print("ERROR", error)
            return
        }
        
        let name = form.rowBy(tag: "name")!.value ?? ""
        let descriptionText = form.rowBy(tag: "descriptionText")!.value ?? ""
        let max = form.rowBy(tag: "max")!.value ?? 1
        let dueDate = form.rowBy(tag: "dueDate")!.value ?? Date()
        let isEveryday = form.rowBy(tag: "isEveryday")!.value ?? false
        let isRecurring = form.rowBy(tag: "isRecurring")!.value ?? false
        let canSendNotifications = form.rowBy(tag: "canSendNotifications")!.value ?? false
        let notificationTime = form.rowBy(tag: "notificationTime")!.value ?? Date()
        
        let todo = Todo(
            name: name,
            descriptionText: descriptionText,
            max: max,
            dueDate: dueDate,
            isRecurring: isRecurring,
            isEveryDay: isEveryday,
            canSendNotifications: canSendNotifications,
            notificationTime: notificationTime
        )
        
        do {
            let realm = try Realm()
            try! realm.write {
                realm.add(todo)
                NotificationCenter.default.post(name: NSNotification.Name(rawValue: "ADD_TODO"), object: nil)
            }
        } catch let error as NSError {
            print("Realm Save Error", error)
        }
        closeModal()
    }
    
    func updateTodo() {
        
    }
    
    func closeModal() {
        dismiss(animated: true) {
            print("modal dismissed!")
        }
    }

}
