//
//  MainVC.swift
//  SaxaRemind
//
//  Created by Alan Sax on 1/25/19.
//  Copyright © 2019 Alan Sax. All rights reserved.
//

import UIKit
import RealmSwift

private let reuseIdentifier = "TodoCell"

class TodosCollectionVC: UICollectionViewController, UICollectionViewDelegateFlowLayout {
    
    var data: [Todo] = [Todo]()
    let realm = try! Realm()

    override func viewDidLoad() {
        super.viewDidLoad()
        view.backgroundColor = .clear
        collectionView?.backgroundColor = .clear
        self.collectionView!.register(TodoCell.self, forCellWithReuseIdentifier: reuseIdentifier)
        collectionView?.contentInset.top = 15
        
        loadData()

        // Notification Center
        NotificationCenter.default.addObserver(self, selector: #selector(self.loadData), name: NSNotification.Name(rawValue: "ADD_TODO"), object: nil)
        
    }
    
    @objc func loadData() {
        print("load data!")
        let results = realm.objects(Todo.self)
        data = Array(results)
        collectionView?.reloadData()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
    }


    override func numberOfSections(in collectionView: UICollectionView) -> Int {
        return 1
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat {
        return 20
    }


    override func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return data.count
    }
    
    override func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: reuseIdentifier, for: indexPath) as! TodoCell
    
        cell.todo = data[indexPath.item]
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return CGSize(width: view.frame.width, height: 90)
    }

}
