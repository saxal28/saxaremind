//
//  HomeVC.swift
//  SaxaRemind
//
//  Created by Alan Sax on 2/2/19.
//  Copyright © 2019 Alan Sax. All rights reserved.
//

import UIKit

class TabsVC: UITabBarController {

    override func viewDidLoad() {
        super.viewDidLoad()
    
        let firstViewController = TodosTodayVC()
        firstViewController.tabBarItem = UITabBarItem(title: "Today", image: UIImage(named: "calendar"), selectedImage: UIImage(named: "calendar-active"))
        
        let secondViewController = TodosUpcomingVC()
        secondViewController.tabBarItem = UITabBarItem(title: "Upcoming", image: UIImage(named: "calendar"), selectedImage: UIImage(named: "calendar-active"))
        
        let tabBarList = [firstViewController, secondViewController]
        
        tabBar.backgroundColor = UIColor.clear
        tabBar.backgroundImage = UIImage()
        tabBar.shadowImage = UIImage()
        tabBar.tintColor = UIColor.todoProgressColor
        
        viewControllers = tabBarList
        
        view.backgroundColor = .backgroundColor
    }

}
