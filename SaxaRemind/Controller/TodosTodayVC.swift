//
//  SecondViewController.swift
//  SaxaRemind
//
//  Created by Alan Sax on 2/2/19.
//  Copyright © 2019 Alan Sax. All rights reserved.
//

import UIKit

class TodosTodayVC: UIViewController {
    
    let header: UIView = {
        let header = UIView()
        header.translatesAutoresizingMaskIntoConstraints = false
        header.heightAnchor.constraint(equalToConstant: 190).isActive = true
        return header
    }()
    
    let todoCollectionViewContainer: UIView = {
        let view = UIView()
        view.translatesAutoresizingMaskIntoConstraints = false
        return view
    }()

    override func viewDidLoad() {
        super.viewDidLoad()
        view.addSubview(header)
        view.addSubview(todoCollectionViewContainer)
        
        header.topAnchor.constraint(equalTo: view.topAnchor).isActive = true
        header.leftAnchor.constraint(equalTo: view.leftAnchor).isActive = true
        header.rightAnchor.constraint(equalTo: view.rightAnchor).isActive = true
        
        todoCollectionViewContainer.topAnchor.constraint(equalTo: header.bottomAnchor).isActive = true
        todoCollectionViewContainer.bottomAnchor.constraint(equalTo: view.bottomAnchor).isActive = true
        todoCollectionViewContainer.leadingAnchor.constraint(equalTo: view.leadingAnchor).isActive = true
        todoCollectionViewContainer.trailingAnchor.constraint(equalTo: view.trailingAnchor).isActive = true
        
        
        let headerController = HeaderVC()
        headerController.view.frame = header.bounds
        addChildViewController(headerController)
        header.addSubview(headerController.view)
    
        let layout = UICollectionViewFlowLayout()
        let todosController = TodosCollectionVC(collectionViewLayout: layout)
        todosController.view.frame = todoCollectionViewContainer.bounds
        
        addChildViewController(todosController)
        todoCollectionViewContainer.addSubview(todosController.view)
        todosController.didMove(toParentViewController: self)
    }
    
}
