//
//  Todo.swift
//  SaxaRemind
//
//  Created by Alan Sax on 1/25/19.
//  Copyright © 2019 Alan Sax. All rights reserved.
//

import Foundation
import RealmSwift
import Realm

class Todo: Object {
    @objc dynamic var name: String = ""
    @objc dynamic var descriptionText: String = ""
    @objc dynamic let progress: Double = 0.0
    
    @objc dynamic var max = 1
    @objc dynamic var current = 0
    
    @objc dynamic var completed: Bool = false
    @objc dynamic let id:String = NSUUID().uuidString
    
    @objc dynamic var dueDate = Date()
    @objc dynamic var isRecurring = false
    @objc dynamic var isEveryDay = false
    
    @objc dynamic var canSendNotifications = false
    @objc dynamic var notificationTime: Date = Date()
    
    required convenience init(
        name: String,
        descriptionText: String,
        max: Int,
        dueDate: Date,
        isRecurring: Bool,
        isEveryDay: Bool,
        canSendNotifications: Bool,
        notificationTime: Date
    ) {
        self.init()
        self.name = name
        self.max = max
        self.descriptionText = descriptionText
        self.dueDate = dueDate
        self.isRecurring = isRecurring
    }
    
}
